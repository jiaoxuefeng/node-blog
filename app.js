const querystring = require('querystring')
const {get, set } = require('./src/db/redis')
const { access } = require('./src/utils/log')

const handleBlogRouter = require('./src/router/blog')
const handleUserRouter = require('./src/router/user')

const SESSION_DATA = {}
// 用于处理 Post data
const getPostData = (req) => {
    const promise = new Promise((resolve, reject) => {
        if (req.method !== 'POST') {
            resolve({})
            return
        }
        if (req.headers['content-type'] !== 'application/json'){
            resolve({})
            return
        }
        let postData = ''
            req.on('data', chunk => {
            postData += chunk.toString()
        })
        req.on('end', () => {
        if(!postData){
            resolve({})
            return
        }
        resolve(
            JSON.parse(postData)
        )
        })
    })
    return promise
}

const getCookieExpire = () => {
    const d = new Date()
    d.setTime(d.getTime() + (24 * 60 * 60 *1000))
    return d.toGMTString()
}

const serverHandle = (req, res) => {
    // 
    access(`${req.method}-- ${req.url} -- ${req.headers['user-agent']} -- ${Date.now()}`)
  // 设置返回格式
    res.setHeader('Content-Type', 'application/json')

    // 处理路径
    const url = req.url
    req.path = url.split('?')[0];

    // 获取query
    req.query = querystring.parse(url.split('?')[1])

    // 解析cookie
    req.cookie = {}
    const cookieStr = req.headers.cookie || ''
    cookieStr.split(';').forEach(item => {
        if(!item){
            return
        }
        const arr = item.split('=')
        const key = arr[0].trim()
        const val = arr[0].trim()
        req.cookie[key] = val
    });
    // 解析session
    let needSetCookie = false
    let userid = req.cookie.userid
    if(userid){
        if (SESSION_DATA[userid]){
            SESSION_DATA[userid] = {}
        }
    }else{
        needSetCookie = true
        userid = `${Date.now()}_${Math.random()}`
        SESSION_DATA[userid] = {}
    }
    req.session = SESSION_DATA[userid]

    getPostData(req).then(postData => {
        req.body = postData

        // 处理blog 路由
        /* const blogData = handleBlogRouter(req, res)
        if (blogData) {
        res.end(
            JSON.stringify(blogData)
        )
        return
        } */
        const blogResult = handleBlogRouter(req, res)
        if(blogResult){
            blogResult.then(blogData => {
                if (needSetCookie){
                    res.setHeader('Set-Cookie', `userid=${userid}; path=/; httpOnly; expires=${getCookieExpire()}`)
                }
                res.end(
                JSON.stringify(blogData)
                )
            })
            return
        }

    // 处理user 路由
    const userResult = handleUserRouter(req, res)
        if (userResult) {
            userResult.then(userData => {
                
                res.end(
                    JSON.stringify(userData)
                )
            })
            return
        }

    // 如果未命中，返回404
        res.writeHead(404, 'Content-Type', 'text/plain')
        res.write('404 Not Founde\n')
        res.end()
    })
}

module.exports = serverHandle