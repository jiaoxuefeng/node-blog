const xss = require('xss')
const { exec } = require('../db/mysql')
const getList = (author, keyword) => {
    let sql = `select * from blogs where 1=1 `
    if(author) {
        sql += `and author='${author}'`
    }
    if(keyword) {
        sql += `and title like '%${keyword}%'`
    }
    sql += `order by createtime desc`
  // 返回的promise
    return exec(sql)
}

const getDetail = (id) => {
    let sql = `select * from blogs where id='${id}'`
    return exec(sql).then(row => {
        return row[0]
    })
}

const newBlog = (blogData = {}) => {
    const author = blogData.author
    const title = xss(blogData.title)
    const content = xss(blogData.content)
    const createTime = Date.now()
    const sql = `insert into blogs(author, title, content, createTime)values('${author}', '${title}', '${content}', ${createTime})`
    return exec(sql).then(data => {
        return {
            id: data.insertId
        }
    })
}

const updateBlog = (id, updateBlog = {}) => {
    const title = xss(updateBlog.title)
    const content = xss(updateBlog.content)
    const sql = `update blogs set title='${title}', content='${content}' where id='${id}'`
    return exec(sql).then(updateData => {
        if(updateData.affectedRows > 0){
            return true
        }
        return false
    })
}

const deleteBlog = (id, author) => {
    const sql = `delete from blogs where id='${id}' and author='${author}'`
    return exec(sql).then(delData => {
        if(delData.affectedRows > 0){
            return true
        }
        return false
    })
}

module.exports = {
    getList,
    getDetail,
    newBlog,
    updateBlog,
    deleteBlog
}