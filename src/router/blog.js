// 请求的数据
const { getList, getDetail, newBlog, updateBlog, deleteBlog } = require('../controller/blog')
// 成功 失败的 model
const { SucessModel, ErrorModel } = require('../model/resModel')

// 登录验证函数
const loginCheck = (req) => {
    if(!req.session.username){
        return Promise.resolve(
            new ErrorModel('尚未登录')
        )
    }
}

const handleBlogRouter = (req, res) => {
    const method = req.method;
    const id = req.query.id || '';
  // 获取博客文章列表
    if (method === 'GET' && req.path === '/api/blog/list') {
        let author =  req.query.author || '';
        const keyword = req.query.keyword || '';

        if(req.query.isadmin){
            const loginCheckResult = loginCheck(req)
            if (loginCheckReuslt) {
                return loginCheckResult
            }
            // 强制查询自己的博客
            author = req.session.username
        }
        const result = getList(author, keyword)
        return result.then(listData => {
            return new SucessModel(listData)
        })
    }
  // 获取文章详细信息
    if (method === 'GET' && req.path === '/api/blog/detail') {
    
        const detailResult = getDetail(id)
        return detailResult.then(data => {
            return new SucessModel(data)
        })
    }
  // 新建一个博客
    if (method === 'POST' && req.path === '/api/blog/new') {
        const loginCheckReuslt = loginCheck(req)
        if(loginCheckReuslt){
            // 未登录
            return loginCheck
        }
        req.body.author = req.session.username
        const newData = newBlog(req.body)
        return newData.then(data => {
            return new SucessModel(data)
        })
    }
    // 更细一篇文章
    if (method === 'POST' && req.path === '/api/blog/update') {
        const loginCheckReuslt = loginCheck(req)
        if (loginCheckReuslt) {
            // 未登录
            return loginCheck
        }
        const result = updateBlog(id, req.body)
        return result.then(updateData => {
            if (updateData) {
                return new SucessModel(result)
            } else {
                return new ErrorModel('更新文章失败')
            }
        })
    }
    // 删除一篇文章
    if (method === 'POST' && req.path === '/api/blog/delete') {
        const loginCheckReuslt = loginCheck(req)
        if (loginCheckReuslt) {
            // 未登录
            return loginCheck
        }
        const author = req.session.username
        const result = deleteBlog(id, author)
        return result.then(delData => {
            if (delData) {
                return new SucessModel()
            } else {
                return new ErrorModel('删除博客失败')
            }
        })
    }
}
module.exports = handleBlogRouter