const { login } = require('../controller/user')
const { SucessModel, ErrorModel } = require('../model/resModel')
const { set } =require('../db/redis')

const handleUserRouter = (req, res) => {
    const method = req.method

  // 登录接口
    if( method === 'POST' && req.path === '/api/user/login' ){
        const { username, password } = req.body
        const result = login(username, password)
        return result.then(data => {
            if(data.username) {
                req.session.username = data.username
                req.session.realname = data.realname
                // 同步 redis
                set(req.sessionId, req.session)
                return new SucessModel()
            }
            return new ErrorModel('登录失败')
        })
    }
}

module.exports = handleUserRouter