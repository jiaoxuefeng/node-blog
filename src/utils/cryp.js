const crypto = require('crypto');

const CECRET_KEY = 'Jxf_0422#';

function md5(content){
  let md5 = crypto.createHash('md5');
  return md5.update(content).digest('hex');
}

function setPassword(password){
  const pwd = `password=${password}&key=${CECRET_KEY}`
  return md5(pwd)
}

module.exports = {
  setPassword
}
